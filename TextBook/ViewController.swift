//
//  ViewController.swift
//  TextBook
//
//  Created by Anton Belousov on 25/05/2018.
//  Copyright © 2018 kp. All rights reserved.
//

import UIKit
import Intents

class ViewController: UITableViewController {

    var texts = [String]()
    var addingText = false
    
    let userDefaults = UserDefaults(suiteName: "group.com.xnet.myapp.ios")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        INPreferences.requestSiriAuthorization {
            (status) in print(status, status == .denied ? "denied" : "\(status.rawValue)" )
        }
        
        texts = userDefaults.stringArray(forKey: "texts") ?? []
        if texts.isEmpty {
            texts = [
                "Core Animation", "Core Graphics", "Core Data"
            ]
            saveTexts()
        }
        tableView.reloadData()
    }
    
    func saveTexts() {
        userDefaults.set(texts, forKey: "texts")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return texts.count + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == texts.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "add_cell", for: indexPath)
            cell.viewWithTag(10)?.isHidden = addingText
            cell.viewWithTag(20)?.isHidden = !addingText
            (cell.viewWithTag(20) as! UITextField).delegate = self
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "text_cell", for: indexPath)
        (cell.viewWithTag(10) as! UILabel).text = texts[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !addingText && indexPath.row == texts.count {
            let cell = tableView.cellForRow(at: indexPath)
            addingText = true
            cell?.viewWithTag(10)?.isHidden = addingText
            cell?.viewWithTag(20)?.isHidden = !addingText
            cell?.viewWithTag(20)?.becomeFirstResponder()
        }
    }
}

extension ViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, !text.isEmpty {
            texts.append(text)
        }
        textField.text = ""
        
        saveTexts()
        addingText = false
        tableView.reloadData()
    }
}
