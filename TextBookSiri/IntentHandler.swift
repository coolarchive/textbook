//
//  IntentHandler.swift
//  TextBookSiri
//
//  Created by Anton Belousov on 25/05/2018.
//  Copyright © 2018 kp. All rights reserved.
//

import Intents
class IntentHandler: INExtension, INSearchForNotebookItemsIntentHandling {
    
    override func handler(for intent: INIntent) -> Any {
        return self
    }

    func resolveItemType(for intent: INSearchForNotebookItemsIntent, with completion: @escaping (INNotebookItemTypeResolutionResult) -> Void) {
        print(intent.content, intent.title, intent.notebookItemIdentifier)
        completion(.success(with: intent.itemType))
    }
    
    func handle(intent: INSearchForNotebookItemsIntent, completion: @escaping (INSearchForNotebookItemsIntentResponse) -> Void) {
        guard let title = intent.title?.spokenPhrase ?? intent.content else {
            completion(INSearchForNotebookItemsIntentResponse(code: .failure, userActivity: nil))
            return
        }
        let texts = UserDefaults(suiteName: "group.com.xnet.myapp.ios")!.stringArray(forKey: "texts") ?? []
        print(texts)
        let items = texts.filter{
            $0.lowercased().contains(title.lowercased())
        }
        guard items.count > 0 else {
            completion(INSearchForNotebookItemsIntentResponse(code: .failure, userActivity: nil))
            return
        }
        
        let response = INSearchForNotebookItemsIntentResponse(code: .success, userActivity: nil)
        response.notes = items.map{
            INNote(title: INSpeakableString(spokenPhrase: $0),
                   contents: [],
                   groupName: nil,
                   createdDateComponents: nil,
                   modifiedDateComponents: nil,
                   identifier: $0)
        }
        
        completion(response)
    }
}

